// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRagdoll);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFreeze);
UCLASS()
class PERSONALPROJECT_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AEnemyCharacter();

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void OnHitCallback(AActor* aDamagedActor, float aDamage, const UDamageType* aDamageType, AController* aInstigatedBy, AActor* aDamageCauser);
	
	UPROPERTY(EditDefaultsOnly)
		class UBoxComponent* mpcollisionBox;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FOnRagdoll mOnRagdollDelegate;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FOnFreeze mOnFreezeDelegate;

	float mHealth = 100.0f;

protected:
	virtual void BeginPlay() override;
	
};
