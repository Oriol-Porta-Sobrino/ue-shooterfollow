// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

class UTP_PickUpComponent;
class UTP_WeaponComponent;
class APersonalProjectCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAmmoChanged, int, aCurrentAmmo);

UENUM()
enum class EWeaponNames : uint8
{
	NONE UMETA(DisplayName = "None"),
	ICE UMETA(DisplayName = "Ice"),
	WIND UMETA(DisplayName = "Wind"),
	SLEEP UMETA(DisplayName = "Sleep")
};

UCLASS()
class PERSONALPROJECT_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeapon();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void BindWeapon(APersonalProjectCharacter* apCharacter);

	void BindActions();
	
	void UnbindActions();

	void AddAmmo();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UTP_WeaponComponent* mpWeaponComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UTP_PickUpComponent* mpPickUpComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		USkeletalMeshComponent* mpMesh;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		EWeaponNames mWeaponName {EWeaponNames::NONE};

	UPROPERTY(BlueprintReadWrite)
		int mBullets {5};

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnAmmoChanged evOnAmmoChanged;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	

};
