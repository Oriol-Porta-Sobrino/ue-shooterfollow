#include "PersonalProject/Public/EnemyCharacter.h"

#include "Components/BoxComponent.h"

AEnemyCharacter::AEnemyCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	mpcollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	mpcollisionBox->SetupAttachment(RootComponent);
	OnTakeAnyDamage.AddDynamic(this, &AEnemyCharacter::OnHitCallback);
}

void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyCharacter::OnHitCallback(AActor* aDamagedActor, float aDamage, const UDamageType* aDamageType,
	AController* aInstigatedBy, AActor* aDamageCauser)
{
	/*mHealth -= aDamage;
	if (mHealth <= 0)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Black, FString::Printf(TEXT("Player %s ha muerto a manos de %s con el arma %s"), *aDamagedActor->GetName(), *aInstigatedBy->GetName(), *aDamageCauser->GetName()));
		Destroy();
	}*/
}

