// Copyright Epic Games, Inc. All Rights Reserved.

#include "PersonalProject/Public/PersonalProjectGameMode.h"
#include "PersonalProject/Public/PersonalProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

APersonalProjectGameMode::APersonalProjectGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
