// Copyright Epic Games, Inc. All Rights Reserved.

#include "PersonalProject/Public/PersonalProjectCharacter.h"
#include "PersonalProject/Public/PersonalProjectProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "TP_PickUpComponent.h"
#include "Weapon.h"
#include "AssetRegistry/Private/AssetRegistryImpl.h"
#include "PersonalProject/Public/Utils.h"


//////////////////////////////////////////////////////////////////////////
// APersonalProjectCharacter

APersonalProjectCharacter::APersonalProjectCharacter()
{
	// Character doesnt have a rifle at start
	bHasRifle = false;
	
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);
		
	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-10.f, 0.f, 60.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	//Mesh1P->SetRelativeRotation(FRotator(0.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-30.f, 0.f, -150.f));

}

void APersonalProjectCharacter::AddWeapon(AWeapon* apWeapon)
{
	if(auto weaponBP {mWeapons.Find(apWeapon->mWeaponName)}; weaponBP)
	{
		AddAmmoToWeapon(apWeapon);
		apWeapon->Destroy();
	}
	else
	{
		mWeapons.Add(apWeapon->mWeaponName, apWeapon);
		apWeapon->BindWeapon(this);
		ChangeWeapon(mWeapons.Num() - 1);
		mpWeaponEquipped->evOnAmmoChanged.Broadcast(mpWeaponEquipped->mBullets);
	}
}

void APersonalProjectCharacter::AddAmmoToWeapon(AWeapon* apWeapon)
{
	TArray<AWeapon*> Weapons;
	mWeapons.GenerateValueArray(Weapons);
	for (int i = 0; i < Weapons.Num(); i++) {
		if (Weapons[i]->mWeaponName == apWeapon->mWeaponName)
		{
			Weapons[i]->AddAmmo();
		}
	}
}

void APersonalProjectCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

//////////////////////////////////////////////////////////////////////////// Input

void APersonalProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		//Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		//Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &APersonalProjectCharacter::Move);

		//Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &APersonalProjectCharacter::Look);

		//Change weapons
		EnhancedInputComponent->BindAction(Weapon1Action, ETriggerEvent::Triggered, this, &APersonalProjectCharacter::ChangeWeapon, 0);
		EnhancedInputComponent->BindAction(Weapon2Action, ETriggerEvent::Triggered, this, &APersonalProjectCharacter::ChangeWeapon, 1);
		EnhancedInputComponent->BindAction(Weapon3Action, ETriggerEvent::Triggered, this, &APersonalProjectCharacter::ChangeWeapon, 2);
	}
}

void APersonalProjectCharacter::ChangeWeapon(int aWeaponIndex)
{
	if(aWeaponIndex >= mWeapons.Num()) return;
	
	TArray<AWeapon*> Weapons;
	mWeapons.GenerateValueArray(Weapons);
	
	AWeapon* Weapon {Weapons.GetData()[mCurrentWeapon]};
	if(mCurrentWeapon >= 0 && Weapon)
	{
		Weapon->UnbindActions();
	}

	mCurrentWeapon = aWeaponIndex;
	Weapon = Weapons.GetData()[aWeaponIndex];
	mpWeaponEquipped = Weapon;

	if(Weapon)
	{
		Weapon->BindActions();
	}
}

void APersonalProjectCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add movement 
		AddMovementInput(GetActorForwardVector(), MovementVector.Y);
		AddMovementInput(GetActorRightVector(), MovementVector.X);
	}
}

void APersonalProjectCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void APersonalProjectCharacter::SetHasRifle(bool bNewHasRifle)
{
	bHasRifle = bNewHasRifle;
}

bool APersonalProjectCharacter::GetHasRifle()
{
	return bHasRifle;
}