#include "Weapon.h"

#include "TP_PickUpComponent.h"
#include "TP_WeaponComponent.h"
#include "PersonalProject/Public/Utils.h"

AWeapon::AWeapon()
{
	PrimaryActorTick.bCanEverTick = true;

	mpWeaponComponent = CreateDefaultSubobject<UTP_WeaponComponent>(TEXT("WeaponData"));
	mpWeaponComponent->SetupAttachment(RootComponent);

	mpPickUpComponent = CreateDefaultSubobject<UTP_PickUpComponent>(TEXT("Pickup"));
	mpPickUpComponent->SetupAttachment(mpWeaponComponent);

	mpMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	mpMesh->SetupAttachment(mpPickUpComponent);
}

void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	mpWeaponComponent->mpOwnerWeapon = this;
}

void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWeapon::BindWeapon(APersonalProjectCharacter* apCharacter)
{
	mpWeaponComponent->AttachWeapon(apCharacter);
}

void AWeapon::BindActions()
{
	mpWeaponComponent->SetVisibility(true, true);
	mpWeaponComponent->BindWeapon();
}

void AWeapon::UnbindActions()
{
	mpWeaponComponent->SetVisibility(false, true);
	mpWeaponComponent->UnbindWeapon();
}

void AWeapon::AddAmmo()
{
	mBullets += mpPickUpComponent->mAmmo;
	evOnAmmoChanged.Broadcast(mBullets);
}
